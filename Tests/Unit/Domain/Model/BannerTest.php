<?php
namespace Iw\CookieBanner\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Michael Blunck  <michael.blunck@phth.de>
 * @author Benjamin Gries <gries@iwkoeln.de>
 */
class BannerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \Iw\CookieBanner\Domain\Model\Banner
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Iw\CookieBanner\Domain\Model\Banner();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function dummyTestToNotLeaveThisFileEmpty()
    {
        self::markTestIncomplete();
    }
}
