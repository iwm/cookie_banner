var expires;
var date;

function setPositiveCookie() {
    var el = document.getElementById('cookieBanner');
    date = new Date();
    date.setTime(date.getTime() + (cookieDuration * 24 * 60 * 60 * 1000));
    expires = "; expires=" + date.toGMTString();
    var jsonString = JSON.stringify({analytics: 1});
    document.cookie = cookieName + "=" + jsonString + "; expires=" + expires + "; path=/";
    el.style.display = 'none';
}

function setNegativeCookie() {
    date = new Date();
    date.setTime(date.getTime() + (cookieDuration * 24 * 60 * 60 * 1000));
    expires = "; expires=" + date.toGMTString();
    var jsonString = JSON.stringify({analytics: 0});
    document.cookie = cookieName + "=" + jsonString + "; expires=" + expires + "; path=/";
    el.style.display = 'none';
}

function getCookie(cookieName) {
    var name = cookieName + "=";
    var cookieArray = document.cookie.split(';');

    for (var i = 0; i < cookieArray.length; i++) {
        var c = cookieArray[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return false;
}

window.onload = function () {
    var el = document.getElementById('cookieBanner');
    if (el !== null && getCookie('iwgdpr') === false) {
        el.style.display = 'block';
    } else {
        enableAnalytics();
    }
};
