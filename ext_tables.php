<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Iw.CookieBanner',
            'Cookiebanner',
            'CookieBanner'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('cookie_banner', 'Configuration/TypoScript', 'Cookie Banner');
    }
);
