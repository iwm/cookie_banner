<?php

namespace Iw\CookieBanner\Controller;

use TYPO3\CMS\Core\Utility\GeneralUtility;

/***
 *
 * This file is part of the "Cookie Banner" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Michael Blunck  <michael.blunck@phth.de>, PHTH
 *           Benjamin Gries <gries@iwkoeln.de>, IwMedien
 *
 ***/


/**
 * BannerController
 */
class BannerController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * @var string
     */
    protected $piwiktracking = '';

    /**
     *
     */
    public function initializeShowAction()
    {
        parent::initializeAction();
        if ($this->settings['banner']['general']['enableCookieBannerCss']) {
            $this->response->addAdditionalHeaderData(
                '<link rel="stylesheet" type="text/css" 
                       href="typo3conf/ext/cookie_banner/Resources/Public/Css/styles.css">'
            );
            $this->response->addAdditionalHeaderData('
        <style>
            .tx-cookie-banner {
                background: ' . $this->settings['banner']['color']['backgroundColor'] . ';
                color: ' . $this->settings['banner']['color']['textColor'] . ';
            }
            .tx-cookie-banner p .btn-accept {
               background: ' . $this->settings['banner']['color']['backgroundColorButton'] . ';
               color: ' . $this->settings['banner']['color']['textColorButton'] . ';
            }
            .tx-cookie-banner p .btn-more {
               background: ' . $this->settings['banner']['color']['backgroundColorLink'] . ';
               color: ' . $this->settings['banner']['color']['textColorLink'] . ';
            }
        </style>');
        }


        // @TODO Multiple domains for piwik
        $footerJs = '
            <script>
                var cookieDuration = ' . $this->settings['banner']['cookie']['cookieDuration'] . ';
                var cookieName = "iwgdpr";
                var analyticsCode = "' . $this->settings['banner']['cookie']['code'] . '";
                var domain = "' . $this->settings['banner']['cookie']['domain'] . '";
                var analyticsDomain = "' . $this->settings['banner']['cookie']['analyticsDomain'] . '";
                var anaylticsId = "' . $this->settings['banner']['cookie']['anaylticsId'] . '";
            </script>
        ';

        if ($this->settings['banner']['cookie']['vendor'] == 'piwik' && $this->settings['banner']['cookie']['vendor'] == 'matomo') {
            $analyticsDomain = $this->settings['banner']['cookie']['analyticsDomain'];
            if (substr($analyticsDomain, -1) !== '/') {
                $analyticsDomain = trim($analyticsDomain) . '/';
            }
            $this->piwiktracking = '<noscript><p><img src="' . $analyticsDomain . 'piwik.php?idsite=' .
                $this->settings['banner']['cookie']['code'] .
                '" style="border:0;" alt="" /></p></noscript>';
        }

        $GLOBALS['TSFE']->additionalHeaderData['tx_' . $this->request->getControllerExtensionKey()] = $footerJs;

        foreach (explode(',', $this->settings['banner']['cookie']['vendor']) as $vendor) {
            $GLOBALS['TSFE']->additionalFooterData['tx_' . $this->request->getControllerExtensionKey()] .=
                '<script about="' . $vendor . '">' . GeneralUtility::getUrl('typo3conf/ext/cookie_banner/Resources/Public/Js/' . $vendor . '.js') .
                '</script>';
        }

    }

    /**
     * action show
     *
     * @return void
     */
    public function showAction()
    {
        $this->view->assignMultiple(
            [
                'pwikitracking' => $this->piwiktracking,
                'settings' => $this->settings,
                'uid' => $GLOBALS['TSFE']->id,
            ]
        );
    }
}
